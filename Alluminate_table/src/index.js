import React from 'react';
import ReactDOM from 'react-dom';
import TableData from './js/component/tableData';

import '../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import './css/index.css';



ReactDOM.render(
    <TableData />, document.getElementById('root')
);

