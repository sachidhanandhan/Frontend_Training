import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

export default class TableData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datum: [
        {
          coin: {
            img: `${require('../../img/bitcoin.png')}`,
            name: 'Bitcoin'
          },
          sector: 'Finance',
          count: 4315,
          price: 2342.34,
          totalValue: 209342.34,
          hrs: 4.06,
          days: 12.09,
          month: 45.09,
          year: 45.09,
          tradingVolume: 71470931705,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            img: `${require('../../img/ethereum.png')}`,
            name: 'Ethereum'
          },
          sector: 'Healthcare',
          count: 307,
          price: 928.23,
          totalValue: 92088.23,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 29124737750,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Bitcoin cash',
            img: `${require('../../img/bit_coin.jpg')}`
          },
          sector: 'RealEstate',
          count: 4315,
          price: 2342.34,
          totalValue: 209342.34,
          hrs: 3.61,
          days: 0.03,
          month: 10.88,
          year: 10.88,
          tradingVolume: 9185387545,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            img: `${require('../../img/ethereum.png')}`,
            name: 'Ethereum'
          },
          sector: 'Healthcare',
          count: 307,
          price: 928.23,
          totalValue: 92088.23,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 29124737750,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Bitcoin cash',
            img: `${require('../../img/bit_coin.jpg')}`
          },
          sector: 'RealEstate',
          count: 4315,
          price: 2342.34,
          totalValue: 209342.34,
          hrs: 3.61,
          days: 0.03,
          month: 10.88,
          year: 10.88,
          tradingVolume: 9185387545,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }

        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            name: 'Ripple',
            img: `${require('../../img/ripple.png')}`
          },
          sector: 'Finance',
          count: 307,
          price: 928.22,
          totalValue: 92118.22,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 8381273646,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            name: 'Litecoin',
            img: `${require('../../img/litecoin.png')}`
          },
          sector: 'Hardware',
          count: 69,
          price: 90.41,
          totalValue: 9980.41,
          hrs: 0.86,
          days: 2.23,
          month: 34.22,
          year: 34.22,
          tradingVolume: 3670158279,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            name: 'Dash',
            img: `${require('../../img/dash.png')}`
          },
          sector: 'RealEstate',
          count: 325,
          price: 87.18,
          totalValue: 807.18,
          hrs: 0.98,
          days: 1.22,
          month: 5.56,
          year: 5.56,
          tradingVolume: 2346489000,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        },
        {
          coin: {
            name: 'Monero',
            img: `${require('../../img/monero.png')}`
          }, sector: 'Finance',
          count: 307,
          price: 928.23,
          totalValue: 92088.23,
          hrs: 0.87,
          days: 6.04,
          month: 12.99,
          year: 12.99,
          tradingVolume: 29124737750,
          active: false,
          name: {
            firstName: 'sachin',
            lastName: 's',
            emailId: 'sachusivakumar@gmail.com'
          }
        }
      ]
    };
  }

  percentageFormat(cell) {
    return `${cell}%`;
  }

  numberFormat(cell) {
    return `$${cell.toLocaleString('en-IN')}`;
  }

  company(cell) {
    if (cell.img) {
      return `<img src= ${cell.img} />${cell.name}`;
    }
    return `<img src= ${require('../../img/monero.png')}>${cell}`;
  }

  sectors(fieldValue) {
    return fieldValue.toLowerCase();
  }

  columnClassName(fieldValue) {
    return fieldValue > 1 ? 'pass-percent' : 'fail-percent';
  }

  editRow(index) {
    let data = this.state.datum[index];
    data.active = !data.active;
    this.setState({
      datum: this.state.datum
    });
  }

  deleteRow(index) {
    this.state.datum.splice(index, 1);
    this.setState({
      datum: this.state.datum
    });
  }
  editOrDelete(cell, row, c, index) {
    var editOrSave = row.active ? 'fa fathis.b-save' : 'fa fa-pencil-square-o';
    return (
      <div>
        <i className={editOrSave} id={index} onClick={e => this.editRow(e.target.id)} />
        <i className="fa fa-minus-circle" id={index} onClick={e => this.deleteRow(e.target.id)} />
      </div>
    );
  }
  renderPaginationPanel = (a) => {
    // console.log(a);
  }
  b = (a,b,c) => {
    console.log(b);
    // return {color: 'red'};
  }
  render() {
    const cellEditProp = {
      mode: 'click',
      nonEditableRows: () => {
        return this.state.datum.filter(data => data.active === false).map(data => data.coin);
      },
      blurToSave: true
    };
    const option = {
      paginationSize: 3,
      page: 2,
      paginationPanel: this.renderPaginationPanel
    };
    return (
      <div>
        <BootstrapTable data={this.state.datum} bordered={false} options={option} insertRow cellEdit={cellEditProp}
          pagination>
          <TableHeaderColumn rowSpan="2" isKey dataField="coin" dataAlign="left" headerAlign="center"
            dataFormat={this.company}>Coin</TableHeaderColumn>
          <TableHeaderColumn rowSpan="2" dataField="sector" dataAlign="center" columnClassName={this.sectors}>
            Sector </TableHeaderColumn>
          <TableHeaderColumn rowSpan="2" dataField="count" dataAlign="center">Count </TableHeaderColumn>
          <TableHeaderColumn rowSpan="2" dataField="price" dataAlign="center"tdStyle={this.b} dataFormat={this.numberFormat}>
            Price </TableHeaderColumn>
          <TableHeaderColumn rowSpan="2" dataField="totalValue" dataAlign="center" dataFormat={this.numberFormat}>
            Total value </TableHeaderColumn>
          <TableHeaderColumn dataAlign="center" colSpan="4">Price change</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField="hrs" dataAlign="center" columnClassName={this.columnClassName} dataFormat={this.percentageFormat}>
            24 hrs</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField="days" dataAlign="center" columnClassName={this.columnClassName} dataFormat={this.percentageFormat}>
            7 days</TableHeaderColumn>
          <TableHeaderColumn row="1" dataField="month" dataAlign="center" columnClassName={this.columnClassName} dataFormat={this.percentageFormat}>
            1 month</TableHeaderColumn>          
          <TableHeaderColumn row="1" dataField="year" dataAlign="center" columnClassName={this.columnClassName} dataFormat={this.percentageFormat}>
            1 year</TableHeaderColumn>
          <TableHeaderColumn rowSpan="2" dataField="tradingVolume" dataAlign="center" dataFormat={this.numberFormat}>
            Trading volume</TableHeaderColumn>
          <TableHeaderColumn rowSpan="2" width="50px" dataField="" dataFormat={this.editOrDelete.bind(this)}
            editable={false}/>
        </ BootstrapTable>
          <div className="assist">
              <span data-toggle="modal" data-target="#myModal">
                  <i className="fa fa-plus-circle" />Add Assist
              </span>
          </div>
          <div id="myModal" className="modal fade" role="dialog">
              <div className="modal-dialog">
                  <div className="modal-content">
                      <div className="modal-header">
                          <button type="button" className="close" data-dismiss="modal">&times;</button>
                          <h4 className="modal-title">Modal Header</h4>
                      </div>
                      <div className="modal-body">
                          <p>Some text in the modal.</p>
                      </div>
                      <div className="modal-footer">
                          <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                  </div>

              </div>
          </div>

      </div>
    );
  }
}

