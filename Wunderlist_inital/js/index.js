/* common functions */
var body = document.body;
inputDiv();
createSubTaskDiv();
var listItem = [];
var lists = []

function createList(event) {
    var listName = event.target.value;
    if (event.which == 13 && listName !== "") {
        var index = lists.push({
            name: listName,
            listId: getTime(),
            task: []
        })
        var newList = createListDiv(lists[index - 1]);
        document.getElementById("listName").value = "";
        body.getElementsByClassName("nav")[0].appendChild(newList);
    }
}

function addTask(event) {
    var item = event.target.value;
    if (event.which == 13 && item !== "") {
        var id = getTime();
        var index = listItem.push({
            name: item,
            id: getTime(),
            date: new Date(),
            isStarred: false,
            isFinished: false,
            subTask: []
        });
        var divItem = createTaskDiv(listItem[index - 1]);
        body.getElementsByTagName('input')[1].value = "";
        body.getElementsByClassName('toDo m-30')[0].appendChild(divItem);
    }
}

function subTask(event) {
    var modal = body.getElementsByClassName("modal")[0];
    var headerContent = modal.getElementsByClassName("headerContent")[0],
        taskObject = findObject(event.target.getAttribute("id"));
    modal.style.display = "block";
    headerContent.innerText = event.target.getElementsByClassName("textContent")[0].innerText;
    modal.getElementsByClassName("dueDate")[0].innerText = taskObject.date;
    modal.setAttribute("id", event.target.getAttribute("id"));
    displaySubTask(modal, event.target.getAttribute("id"));
    if (taskObject.isStarred) {
        modal.getElementsByClassName("fa-star")[0].classList.add("yellow-clr");
    } else {
        modal.getElementsByClassName("fa-star")[0].classList.remove("yellow-clr");
    }
    if (taskObject.isFinished) {
        headerContent.classList.add("strike");
    } else {
        headerContent.classList.remove("strike");
    }
}

function displaySubTask(modal, id) {
    var listObject = findObject(id),
        arrayLength = listObject.subTask.length;
    if (modal.getElementsByClassName("subTaskLists")[0]) {
        document.getElementsByClassName("subTaskLists")[0].innerHTML = "";
    }
    for (var length = 0; length < arrayLength; length++) {
        createSubTaskList(listObject.subTask[length]);
    }
}

function finishTask(event) {
    var taskObject = findObject(event.target.parentElement.nextSibling.getAttribute("id")),
        modal = body.getElementsByClassName("modal")[0];
    event.target.parentNode.nextSibling.classList.toggle("strike");
    taskObject.isFinished = event.target.parentNode.nextSibling.classList.contains("strike");
    if (taskObject.id == modal.getAttribute("id")) {
        modal.getElementsByClassName("headerContent")[0].classList.toggle("strike");
        modal.getElementsByTagName("input")[0].checked;
    }
}

function finishSubTask(event) {
    event.target.nextSibling.classList.toggle("strike");
}

function starred(event) {
    var taskObject = findObject(event.target.parentElement.previousSibling.getAttribute("id")),
        modal = body.getElementsByClassName("modal")[0];
    event.target.classList.toggle("yellow-clr");
    taskObject.isStarred = event.target.classList.contains("yellow-clr");
    if (modal.getAttribute("id") == taskObject.id) {
        modal.getElementsByClassName("fa fa-star")[0].classList.toggle("yellow-clr");
    }
}

function deleteSubTask(event) {
    if (confirm("Do you want to delete this task")) {
        var subTaskDiv = event.target.parentNode.parentNode,
            id = subTaskDiv.getAttribute("id");
        subTaskDiv.style.display = "none";
        textContent = subTaskDiv.getElementsByClassName("headerContent")[0].textcontent;
        document.getElementById(id).parentElement.remove();
        listItem.pop(textContent);
    }
}

function addSubTask(event) {
    var item = event.target.value,
        modal = event.target.parentElement.parentElement.parentElement.parentElement,
        id = modal.getAttribute("id");
    var items = event.target.parentNode.parentNode.nextSibling.innerText;
    if (event.which === 13 && item !== null) {
        findObject(id).subTask.push(item);
        createSubTaskList(item);
        event.target.value = "";
    }
}

function showStarredTasks() {n
    removeLastTasks();
    listLength = lists.length;
    var starredDiv = 0;
    for (var length = 0; length < listLength; length++) {
        for (var limit = 0; limit < lists[length].task.length; limit++) {
            if (lists[length].task[limit].isStarred) {
                var taskDiv = createTaskDiv(lists[length].task[limit]);
                body.getElementsByClassName('toDo m-30')[0].appendChild(taskDiv);
                body.getElementsByClassName("fa-star")[starredDiv++].classList.add("c-yellow");
            }
            if (lists[length].task[limit].isFinished) {
                body.getElementsByClassName("displayToDo")[length].classList.add("strike");
            }
        }
    }
}

function displayTasks(event) {
    removeLastTasks();
    var listObj = findListObject(event.target.parentElement.getAttribute("listId"));
    listItem = listObj.task;
    document.getElementsByClassName("content-body")[0].style.display = "flex";
    document.getElementById("taskName").placeholder = "Add a task in " + listObj.name + "...";
    document.getElementById("taskName").setAttribute("listId", event.target.parentElement.getAttribute("listId"));
    if (listItem.length > 0) {
        for (length = 0; length < listItem.length; length++) {
            var taskDiv = createTaskDiv(listItem[length]);
            appendDiv({
                parent: body.getElementsByClassName('toDo m-30')[0],
                child: taskDiv
            });
            if (listItem[length].isStarred) {
                body.getElementsByClassName("fa fa-star")[length].classList.add("c-yellow");
            }
            if (listItem[length].isFinished) {
                body.getElementsByClassName("textContent")[length].classList.add("strike");
            }
        }
    }
}

function removeLastTasks() {
    var totalTaskDiv = body.getElementsByClassName("task bdr-rad"),
        divLength = totalTaskDiv.length;
    while (0 < divLength) {
        divLength--;
        body.getElementsByClassName("task bdr-rad")[divLength].remove();
    }
}

/** common methods */
function findObject(id) {
    var arrayListLength = listItem.length
    for (var obj = 0; obj < arrayListLength; obj++) {
        if (listItem[obj].id == id) {
            return listItem[obj];
        }
    }
}

function findListObject(id) {
    var arrayListLength = lists.length;
    for (var obj = 0; obj < arrayListLength; obj++) {
        if (lists[obj].listId == id) {
            return lists[obj];
        }
    }
}

function createElement(element) {
    var elementObj = document.createElement(element.name);
    if (element.attribute) {
        if (element.attribute.class) {
            elementObj.className = element.attribute.class;
        }
        if (element.attribute.data) {
            elementObj.innerText = element.attribute.data;
        }
        if (element.attribute.id) {
            elementObj.id = element.attribute.id;
        }
    }
    if (element.style) {
        if (element.style.cursor) {
            elementObj.style.cursor = element.style.cursor;
        }
    }
    return elementObj;
}

function appendDiv(append) {
    append.parent.appendChild(append.child);
}

function bind(element, event, func) {
    if (element.addEventListener) {
        element.addEventListener(event, func);
    }
}

function getTime() {
    return new Date().getUTCMilliseconds();
}

function getDate() {
    return new Date();
}

/** Div creation*/
function createListDiv(listObj) {
    var newList = createElement({
            name: "li"
        }),
        newHref = document.createElement('a'),
        menuIcon = createElement({
            name: "i",
            attribute: {
                class: "fa fa-bars p-10"
            }
        }),
        listName = createElement({
            name: "span",
            attribute: {
                data: listObj.name
            }
        });
    appendDiv({
        parent: newHref,
        child: menuIcon
    });
    appendDiv({
        parent: newHref,
        child: listName
    });
    appendDiv({
        parent: newList,
        child: newHref
    });
    newList.style.cursor = "pointer";
    newList.setAttribute("listId", listObj.listId);
    bind(newList, "click", displayTasks);
    return newList;
}

function inputDiv() {
    var bodyContent = createElement({
            name: "DIV",
            attribute: {
                class: "content-body full-w-h"
            }
        }),
        taskDiv = createElement({
            name: "DIV",
            attribute: {
                class: "toDo m-30"
            }
        }),
        inputDiv = createElement({
            name: "DIV",
            attribute: {
                class: "getTask"
            }
        }),
        taskInput = createElement({
            name: "input",
            attribute: {
                class: "bdr-rad",
                id: "taskName"
            }
        });
    taskInput.type = "text";
    taskInput.placeholder = "Add a note..."
    appendDiv({
        parent: inputDiv,
        child: taskInput
    });
    appendDiv({
        parent: taskDiv,
        child: inputDiv
    });
    appendDiv({
        parent: bodyContent,
        child: taskDiv
    });
    appendDiv({
        parent: body.getElementsByClassName("content")[0],
        child: bodyContent
    });
    bodyContent.style.display = "none";
}

function createTaskDiv(taskObj) {
    var months = ["jan", "Feb", "Mar", "April", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var date = taskObj.date;
    var month = months[date.getMonth()];
    var toDoDiv = createElement({
            name: "DIV",
            attribute: {
                class: "task bdr-rad"
            }
        }),
        checkBoxDiv = createElement({
            name: "DIV",
            attribute: {
                class: "checkbox bdr-right"
            }
        }),
        option = createElement({
            name: "input"
        }),
        displayDiv = createElement({
            name: "DIV",
            attribute: {
                class: "displayToDo p-15"
            },
            style: {
                cursor: "pointer"
            }
        }),
        toDoText = createElement({
            name: "span",
            attribute: {
                class: "textContent",
                data: taskObj.name
            }
        }),
        dateSpan = createElement({
            name: "span",
            attribute: {
                class: "dateSpan",
                data: month + "-" + date.getDate()
            }
        })
    pinIcon = createElement({
            name: "i",
            attribute: {
                class: "fa fa-thumb-tack"
            }
        }),
        starredDiv = createElement({
            name: "DIV",
            attribute: {
                class: "today p-15"
            }
        }),
        starredIcon = createElement({
            name: "i",
            attribute: {
                class: "fa fa-star",
            }
        });
    bind(displayDiv, "dblclick", subTask);
    bind(starredIcon, "click", starred);
    displayDiv.setAttribute("id", taskObj.id);
    option.type = 'checkbox';
    option.onchange = finishTask;
    appendDiv({
        parent: starredDiv,
        child: dateSpan
    });
    appendDiv({
        parent: starredDiv,
        child: starredIcon
    });
    appendDiv({
        parent: checkBoxDiv,
        child: option
    });
    appendDiv({
        parent: toDoDiv,
        child: checkBoxDiv
    });
    appendDiv({
        parent: displayDiv,
        child: toDoText
    });
    appendDiv({
        parent: displayDiv,
        child: pinIcon
    });
    appendDiv({
        parent: toDoDiv,
        child: displayDiv
    });
    appendDiv({
        parent: toDoDiv,
        child: starredDiv
    });
    return toDoDiv;
}

function createSubTaskList(taskname) {
    var textareaLi = createElement({
            name: "li"
        }),
        textareaCheckbox = createElement({
            name: "input",
            attribute: {
                class: "textareaCheckbox"
            }
        }),
        textareaSpan = createElement({
            name: "span",
            attribute: {
                class: "textAreaSpan",
                data: taskname
            }
        });
    textareaCheckbox.type = "checkbox";
    appendDiv({
        parent: textareaLi,
        child: textareaCheckbox
    });
    appendDiv({
        parent: textareaLi,
        child: textareaSpan
    });
    appendDiv({
        parent: document.getElementsByClassName("subTaskLists")[0],
        child: textareaLi
    });
    bind(textareaCheckbox, "change", finishSubTask);
}

function createSubTaskDiv() {
    // header div
    var subTaskDiv = createElement({
            name: "DIV",
            attribute: {
                class: "modal m-30 bdr-rad",
                id: "subTaskDiv"
            }
        }),
        header = createElement({
            name: "DIV",
            attribute: {
                class: "header bdr-rad"
            }
        }),
        headerCheckboxDiv = createElement({
            name: "DIV",
            attribute: {
                class: "header-checkbox"
            }
        }),
        headerBody = createElement({
            name: "DIV",
            attribute: {
                class: "header-body"
            }
        }),
        headerIcon = createElement({
            name: "DIV",
            attribute: {
                class: "header-star"
            }
        }),
        headerCheckbox = createElement({
            name: "input",
            attribute: {
                class: "headerCheckbox"
            }
        });
    headerCheckbox.type = "checkbox";
    headerBody.appendChild(createElement({
        name: "span",
        attribute: {
            class: "headerContent"
        }
    }));
    headerIcon.appendChild(createElement({
        name: "i",
        attribute: {
            class: "fa fa-star"
        }
    }));
    headerCheckboxDiv.appendChild(headerCheckbox);
    header.appendChild(headerCheckboxDiv);
    header.appendChild(headerBody);
    header.appendChild(headerIcon);
    //body div
    var body = createElement({
            name: "DIV",
            attribute: {
                class: "body"
            }
        }),
        ul = createElement({
            name: "ul",
            attribute: {
                class: "noteList"
            }
        }),
        dueList = createElement({
            name: "li",
            attribute: {
                class: "bdr-bottom"
            }
        }),
        timerList = createElement({
            name: "li",
            attribute: {
                class: "bdr-bottom"
            }
        }),
        dueListDiv = createElement({
            name: "DIV",
            attribute: {
                class: "body-calendar"
            }
        });
    timerListDiv = createElement({
        name: "DIV",
        attribute: {
            class: "remindMe"
        }
    });
    dueListDiv.appendChild(createElement({
        name: "i",
        attribute: {
            class: "fa fa-calendar"
        }
    }));
    dueListDiv.appendChild(createElement({
        name: "span",
        attribute: {
            class: "dueDate"
        }
    }));
    dueList.appendChild(dueListDiv);
    timerListDiv.appendChild(createElement({
        name: "i",
        attribute: {
            class: "fa fa-clock-o"
        }
    }));
    timerListDiv.appendChild(createElement({
        name: "span",
        attribute: {
            data: "Remind me"
        }
    }));
    timerList.appendChild(timerListDiv);
    //sub note div
    var subNoteDiv = createElement({
            name: "DIV",
            attribute: {
                class: "subtask"
            }
        }),
        subNoteTextDiv = createElement({
            name: "DIV",
            attribute: {
                class: "textinput"
            }
        }),
        textAreaDiv = createElement({
            name: "DIV",
            attribute: {
                class: "textarea"
            }
        }),
        textareaUl = createElement({
            name: "ul",
            attribute: {
                class: "subTaskLists"
            }
        }),
        subNoteTextInput = createElement({
            name: "input"
        }),
        subNotePlusIcon = createElement({
            name: "DIV",
            attribute: {
                class: "plus bdr-right"
            }
        });
    subNotePlusIcon.appendChild(createElement({
        name: "i",
        attribute: {
            class: "fa fa-plus"
        }
    }));
    textAreaDiv.appendChild(textareaUl);
    subNoteTextInput.type = "text";
    subNoteTextInput.placeholder = "Add a sub task";
    bind(subNoteTextInput, "keypress", addSubTask);
    subNoteTextDiv.appendChild(subNoteTextInput);
    subNoteDiv.appendChild(subNotePlusIcon);
    subNoteDiv.appendChild(subNoteTextDiv);
    //footer div
    var footerDiv = createElement({
            name: "DIV",
            attribute: {
                class: "footer bdr-top"
            }
        }),
        deleteIcon = createElement({
            name: "i",
            attribute: {
                class: "fa fa-trash"
            },
            style: {
                cursor: "pointer"
            }
        });
    footerDiv.appendChild(deleteIcon);
    bind(deleteIcon, "click", deleteSubTask);
    ul.appendChild(dueList);
    ul.appendChild(timerList);
    body.appendChild(ul);
    body.appendChild(subNoteDiv);
    body.appendChild(textAreaDiv);
    subTaskDiv.appendChild(header);
    subTaskDiv.appendChild(body);
    subTaskDiv.appendChild(footerDiv);
    var modal = document.getElementsByClassName("content-body")[0];
    modal.appendChild(subTaskDiv);
    subTaskDiv.style.display = "none";
}

bind(document.getElementById("listName"), "keypress", createList);
bind(document.getElementById("taskName"), "keypress", addTask);
bind(document.getElementById("starredTasks"), "click", showStarredTasks);
