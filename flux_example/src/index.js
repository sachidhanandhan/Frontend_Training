import React from 'react';
import ReactDOM from 'react-dom';
import User from './js/component/user';
import '../node_modules/react-toastify/dist/ReactToastify.min.css';
ReactDOM.render(
  <User />,
  document.getElementById('root')
);
