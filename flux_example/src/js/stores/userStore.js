import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';
import userApi from '../userApi';
import Const from '../constants';
import { errorMessage, successMessage } from '../userMessages';

class UserStore extends EventEmitter {
  constructor() {
    super();
    this.users = [];
    this.message = "";
  }

  getUsers() {
    return this.users;
  }

  emitChange(message) {
    this.emit(message);
  }

  addEventListener(eventName, callback) {
    this.on(eventName, callback);
  }

  removeEventListener(eventName, callback) {
    this.removeListener(eventName, callback)
  }

  handleActions(action) {
    var value = action.data;
    switch (action.type) {
      case Const.CREATE_USER:
        userApi.post(Const.USER_URL, value).then((response) => {
          if (response.status === 200) {
            this.message = successMessage.ADD_SUCCESS;
            this.emitChange(Const.CHANGE);
          } else {
            this.message = errorMessage.NOT_ADDED;
            this.emitChange(Const.ERROR);
          }
        });
        break;
      case Const.FETCH_USER:
        userApi.get(Const.USER_URL).then((value) => {
          this.users = value.data;
          this.emitChange(Const.CHANGE);
        });
        break;
      case Const.DELETE_USER:
        userApi.delete(Const.USER_URL, value).then((response) => {
          if (response.status === 200) {
            this.message = successMessage.DELETE_SUCCESS;
            this.emitChange(Const.CHANGE);
          } else {
            this.message = errorMessage.NOT_DELETED;
            this.emitChange(Const.ERROR);
          }
        });
        break;
      default:
    }
  }
}

const userStore = new UserStore();
dispatcher.register(userStore.handleActions.bind(userStore));
export default userStore;