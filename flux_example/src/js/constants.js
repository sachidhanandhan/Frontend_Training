export default Object.freeze({
    CREATE_USER: "createUser",
    FETCH_USER: "fetchUsers",
    DELETE_USER: "deleteUser",
    USER_URL: "http://localhost:3000/api/userProfiles",
    CHANGE: "change",
    ERROR: "error"
});
