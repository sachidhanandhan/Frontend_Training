import dispatcher from '../dispatcher';
import Const from '../constants';

class UserActions {
  createUser(data) {
    dispatcher.dispatch({
      type: Const.CREATE_USER,
      data: data
    });
  }
  fetchUsers() {
    dispatcher.dispatch({
      type: Const.FETCH_USER
    });

  }
  deleteUser(id) {
    dispatcher.dispatch({
      type: Const.DELETE_USER,
      data: id
    });
  }
}

const userActions = new UserActions();
export default userActions;