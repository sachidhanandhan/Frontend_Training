import React from 'react';
import UserStore from '../stores/userStore';
import UserAction from '../actions/userActions';

export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      message: ""
    };
  }

  componentWillMount() {
    UserStore.addEventListener('change', this.change);
    UserStore.addEventListener('error', this.error);
  }

  componentDidMount() {
    UserAction.fetchUsers();
  }

  componentWillUnmount() {
    UserStore.removeEventListner('change', this.change);
    UserStore.removeEventListener('error', this.error);
  }

  change = () => {
    this.setState({
      users: UserStore.getUsers(),
      message: UserStore.message
    });
  }

  error = () => {
    this.setState({
      message: UserStore.message
    });
  }

  createUser(e) {
    var data = {
      name: e.target.elements["name"].value,
      emailId: e.target.elements["emailId"].value,
      phonenumber: e.target.elements["phonenumber"].value
    }
    UserAction.createUser(data);
  }

  deleteUser(id) {
    UserAction.deleteUser(id);
  }

  render() {
    var { users, message } = this.state;
    return [
      `${message}`,
      <form key="inputForm" onSubmit={(e) => this.createUser(e)}>
        <input type="text" name="name" placeholder="Enter your name" />
        <input type="number" name="phonenumber" placeholder="Enter yout phonenumber" />
        <input type="email" name="emailId" placeholder="Enter your emailId" />
        <input type="submit" value="Submit" />
      </form>,
      <ul key="userList">
        {users.map((user, index) => {
          return <li key={user.id}>{user.name} - {user.id} - {user.emailId} -  {user.phonenumber} <button onClick={(e) => this.deleteUser(user.id)}>Delete</button></li>;
        })}
      </ul>,
    ];
  }
}