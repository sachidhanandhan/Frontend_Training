export const errorMessage = {
    NOT_DELETED : "User not deleted, please try again later",
    NOT_ADDED : "User not added, please try again later"
}

export const successMessage = {
    DELETE_SUCCESS : "User deleted successfully",
    ADD_SUCCESS : "User added successfully"
}