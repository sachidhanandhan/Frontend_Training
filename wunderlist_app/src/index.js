import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import Wrapper from './js/components/Wrapper';
import PageContent from './js/components/PageContent';
import Store from './js/store/Store';

import '../bower_components/font-awesome/css/font-awesome.css';
import '../src/css/index.css';
import '../bower_components/bootstrap/dist/css/bootstrap.min.css';
import '../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css';

const data = [
  {
    id: new Date().valueOf(),
    listName: "Inbox",
    tasks: [
      {
        id: new Date().valueOf(),
        taskName: "sachin",
        date: Date().substr(4, 6),
        starred: false,
        finished: false,
        notes: "",
        subTasks: []
      }
    ]
  }
]

var store = new Store(data);

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={Wrapper}>
      <Route path="/lists/:id" component={PageContent} />
    </Route>
  </Router>,
  document.getElementById('wrapper')
);