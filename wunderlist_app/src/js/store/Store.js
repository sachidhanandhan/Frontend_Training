export default class Store {
  static lists = [];
  constructor(data) {
    Store.lists = data;
  }
  static setList(task) {
    Store.lists.push(task);
  }
  static getLists() {
    return Store.lists;
  }
  static getTask(listId, taskId) {
    for (var length = 0; length < Store.lists.length; length++) {
      if (Store.lists[length].id == listId) {
        var list = Store.lists[length];
        for (var listLength = 0; listLength < list.tasks.length; listLength++) {
          if (list.tasks[listLength].id == taskId) {
            return list.tasks[listLength];
          }
        }
      }
    }
  }
  static getList(id) {
    for (var length = 0; length < Store.lists.length; length++) {
      if (Store.lists[length].id == id) {
        return Store.lists[length];
      }
    }
  }
}