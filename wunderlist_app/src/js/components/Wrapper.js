import React from 'react';

import Sidebar from './Sidebar';
import PageContent from './PageContent';
import Store from '../store/Store';

export default class Wrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: Store.getLists()[0]
    }
  }

  getList(listId) {
    this.state.showSubTask = false;
    this.setState({
      list: Store.getList(listId)
    })
  }

  render() {
    return (
      <div>
        <Sidebar getList={
          listId => this.getList(listId)
        } />
        <PageContent obj={
          {
            listId: this.state.list.id
          }
        } />
      </div>
    );
  }
}