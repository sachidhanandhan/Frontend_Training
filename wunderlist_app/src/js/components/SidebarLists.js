import React from 'react';

export default class SidebarLists extends React.Component {
  setId(id) {
    this.props.obj.getList(id);
  }
  render() {
    var { lists } = this.props.obj;
    return (
      <ul className="sidebar-nav">
        {lists.map((list, index) => {
          return <li key={index} className="ellipsis" onClick={id => this.setId(list.id)}><i className="fa fa-list"></i><span>{list.listName}</span></li>;
        })}
      </ul>
    );
  }
}