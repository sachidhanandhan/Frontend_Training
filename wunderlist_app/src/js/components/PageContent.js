import React from 'react';

import AddTask from './AddTask';
import SubTask from './SubTask';
import Store from '../store/Store';

export default class PageContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: Store.getList(this.props.obj.listId).tasks[0],
      starred: false,
      finished: false,
      showSubTask: false
    }
  }
  getTask(id) {
    this.state.showSubTask =  true;
    this.state.task = Store.getTask(this.props.obj.listId, id);
    this.setState({
      task: this.state.task
    })
  }

  updateTask(id, obj) {
    var task = Store.getTask(this.props.obj.listId, id);
    if (obj.isFinished) {
      task.finished = !task.finished;
      document.getElementById(id).classList.toggle('finished');
      this.state.finished = task.finished;
    }
    if (obj.isStarred) {
      task.starred = !task.starred;
      document.getElementById(id).classList.toggle('starred');
      this.state.starred = task.starred;
    }
    this.setState({
      starred: this.state.starred,
      finished: this.state.finished
    })
  }
  
  deleteTask(task) {
    var list = Store.getList(this.props.obj.listId);
    var index = list.tasks.indexOf(task);
    list.tasks.splice(index, 1);
    this.state.showSubTask = false;
    this.setState({
      showSubTask: this.state.showSubTask
    })
  }  

  render() {
    return (
      <div className="page-content-wrapper">
        <div className="row w-100">
          <div className="col-lg-7 col-sm-12">
            <AddTask obj={
              {
                listId: this.props.obj.listId,
                getTask: id => this.getTask(id),
                updateTask: (id, obj) => this.updateTask(id, obj)
              }
            } />
          </div>
          <div className="col-lg-5 hidden-md col-xs-12 col-sm-12">
            <SubTask obj={
              {
                task: this.state.task,
                showSubTask: this.state.showSubTask,
                deleteTask: task => this.deleteTask(task),
                updateTask: (id, obj) => this.updateTask(id, obj)
              }
            } />
          </div>
        </div>
      </div>
    );
  }
}