import React from 'react';
import SidebarLists from './SidebarLists';
import Store from '../store/Store';

export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lists: Store.getLists()
    };
  }
  addTask(e) {
    if (e.key === 'Enter' && e.target.value !== '') {
      var obj = {
        id: new Date().valueOf(),
        listName: e.target.value,
        tasks: []
      }
      Store.setList(obj);
      this.setState({
        lists: Store.getLists()
      });
      e.target.value = "";
    }
  }

  toggle() {
    document.getElementById("wrapper").classList.toggle('toggled');
  }

  render() {
    return (
      <div className="sidebar-wrapper">
        <div className="menu-toggle">
          <i className="fa fa-bars" onClick={e => this.toggle()}></i>
        </div>
        <div className="list-input">
          <i className="fa fa-plus"></i>
          <input type="text" onKeyPress={e => this.addTask(e)} placeholder="Create a new task" />
        </div>
        <SidebarLists obj={
          {
            lists: this.state.lists,
            getList: this.props.getList
          }
        } />
      </div>
    );
  }
}