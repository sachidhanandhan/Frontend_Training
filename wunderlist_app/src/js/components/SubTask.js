import React from 'react';

export default class SubTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: this.props.obj.task,
      panelHeader: "panel-header",
      classHidden: "panel sub-task hidden",
      subTaskFinished: "finished"
    }
  }

  componentWillReceiveProps(props) {
    this.state.panelHeader = "panel-header";
    this.state.classHidden = "panel sub-task hidden";
    this.state.panelHeader = props.obj.task.finished ? this.state.panelHeader + " finished" : this.state.panelHeader;
    this.state.panelHeader = props.obj.task.starred ? this.state.panelHeader + " starred" : this.state.panelHeader;
    this.state.classHidden = props.obj.showSubTask ? "panel sub-task" : "panel sub-task hidden";
    this.state.task = props.obj.task;
    this.setState({
      task: this.state.task,
      panelHeader: this.state.panelHeader,
      classHidden: this.state.classHidden
    })
  }

  addSubtask(event) {
    if (event.key === 'Enter' && event.target.value !== '') {
      let subtasks = this.state.task.subTasks;
      subtasks.push({
        id: new Date().valueOf(),
        taskName: event.target.value,
        finished: false
      });
      this.setState({
        task: this.state.task
      })
      event.target.value = "";
    }
  }

  deleteSubTask(subTask) {
    var index = this.state.task.subTasks.indexOf(subTask);
    this.state.task.subTasks.splice(index, 1);
    this.setState({
      task: this.state.task
    })
  }

  finishedSubTask(subTask) {
    var index = this.state.task.subTasks.indexOf(subTask);
    this.state.task.subTasks[index].finished = true;
  }

  addNotes(event) {
    if (event.target.value !== '') {
      this.state.task.notes = event.target.value;
      this.setState({
        task: this.state.task
      })
    }
  }

  deleteTask() {
    this.props.obj.deleteTask(this.state.task);
    this.state.classHidden = "panel sub-task hidden";
    this.setState({
      classHidden: this.state.classHidden
    })
  }

  finishedTask(id, obj) {
    this.props.obj.updateTask(id, obj);
  }

  starredTask(id, obj) {
    this.props.obj.updateTask(id, obj);
  }

  render() {
    let { task, panelHeader, classHidden } = this.state;
    return (
      <div className={classHidden}>
        <div className={panelHeader} id="panel-header">
          <div className="col-md-1 col-lg-1">
            <input type="checkbox" className="checkbox" onClick={(id, obj) => this.finishedTask(task.id, { isFinished: true })} />
          </div>
          <div className="col-md-8 col-lg-10 ellipsis">
            <span className="content">{task.taskName}</span>
          </div>
          <div className="col-md-3 col-lg-1">
            <i className="fa fa-star" onClick={(id, obj) => this.starredTask(task.id, { isStarred: true })}></i>
          </div>
        </div>
        <div className="panel-body">
          <ul className="subtask-sidebar">
            <li><i className="fa fa-calendar"></i><input type="text" maxlength="0" placeholder="Due today" className="remainder" /></li>
            <li><i className="fa fa-clock-o"></i><input type="text" maxlength="0" placeholder="Remind me" className="remainder" /></li>
            <li><i className="fa fa-pencil"></i><textarea onChange={e => this.addNotes(e)} id="text-area" placeholder="Add a note" value={task.notes}></textarea></li>
            <li><i className="fa fa-plus"></i><input type="text" placeholder="Add a task" onKeyPress={this.addSubtask.bind(this)} /></li>
            <ul className="subtasks">
              {task.subTasks.map((subTask, index) => {
                return <li key={index} className={this.state.finishedSubTask} id={subTask.id}><input type="checkbox" className="checkbox" onClick={e => this.finishedSubTask(subTask)} /><span>{subTask.taskName}</span><i className="fa fa-close" onClick={e => this.deleteSubTask(subTask)}></i></li>;
              })}
            </ul>
          </ul>
        </div>
        <div className="panel-footer">
          <i className="fa fa-trash-o" onClick={() => this.deleteTask()}></i>
        </div>
      </div>
    );
  }
} ``