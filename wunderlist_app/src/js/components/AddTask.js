import React from 'react';
import TaskLists from './TaskLists';
import Store from '../store/Store';

export default class AddTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: Store.getList(this.props.obj.listId).tasks
    }
  }
  componentWillReceiveProps(props) {
    this.setState({
      tasks: Store.getList(this.props.obj.listId).tasks
    })
  }

  addTask(event) {
    if (event.key === 'Enter' && event.target.value !== '') {
      var task = {
        id: new Date().valueOf(),
        taskName: event.target.value,
        date: Date().substr(4, 6),
        starred: false,
        finished: false,
        notes: "",
        subTasks: []
      }
      this.state.tasks.push(task);
      this.setState({
        taskLists: this.state.tasks
      });
      event.target.value = "";
    }
  }
  render() {
    return (

      <div className="row add-tasks">
        <div className="col-md-12">
          <input type="text" placeholder="Add a to-do..." onKeyPress={e => this.addTask(e)} />
        </div>
        <TaskLists obj={
          {
            tasks: this.state.tasks,
            getTask: this.props.obj.getTask,
            updateTask: this.props.obj.updateTask
          }
        } />
      </div>
    );
  }
}