import React from 'react';

export default class TaskLists extends React.Component {
  setTask(id) {
    this.props.obj.getTask(id);
  }

  finishedTask(id, obj) {
    this.props.obj.updateTask(id, obj);
  }

  starredTask(id, obj) {
    this.props.obj.updateTask(id, obj);
  }

  render() {
    var {tasks} = this.props.obj;
    return (
      <div>
        {tasks.map((task, index) => {
          return (
            <div key={index} className="col-md-12 col-sm-12">
              <div id={task.id} className="row task-list">
                <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                  <input type="checkbox" className="checkbox" onClick={(id, obj) => this.finishedTask(task.id, { isFinished: true })} />
                </div>
                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-8 task-content ellipsis">
                  <span id={task.id} onClick={e => this.setTask(e.target.id)}>{task.taskName}</span>
                </div>
                <div className="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                  <span className="date">{task.date}</span>
                  <i className="fa fa-star" onClick={(id, obj) => this.starredTask(task.id, { isStarred: true })}></i>
                </div>
              </div>
            </div>);
        })}
      </div>
    );
  }
}