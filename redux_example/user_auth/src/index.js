import '../bower_components/bootstrap/dist/css/bootstrap.min.css';
// import '../bower_components/jquery/dist/jquery.min.js';
// import '../bower_components/bootstrap/dist/js/bootstrap.min.js';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore, combineReducers } from 'redux';
import App from './js/containers/userLogin';
import User from './js/containers/users';
import AddUser from './js/containers/addUsers';
import * as Reducers from './js/reducers/reducers';
import thunk from 'redux-thunk';
import { syncHistoryWithStore, routerReducer, routerMiddleware } from 'react-router-redux';
import { Router, Route, browserHistory } from 'react-router';

const reducer = combineReducers({ ...Reducers, routing: routerReducer });
const store = createStore(reducer, applyMiddleware(thunk, routerMiddleware(browserHistory)));
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route exact path="/" component={App} />
      <Route path="user" component={User} />
      <Route path="/addUser" component={AddUser} />
    </Router>
  </Provider>,
  document.getElementById('root')
);