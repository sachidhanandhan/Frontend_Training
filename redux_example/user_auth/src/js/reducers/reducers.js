import constants from '../util/constants';

export const loginFail = (state = [], action) => {
    switch (action.type) {
        case constants.USER_LOGIN_FAIL:
            return action.payload;
        default:
            return state;
    }
}

export const loginSuccess = (state = null, action) => {
    switch (action.type) {
        case constants.USER_LOGIN:
            return action.payload
        default:
            return state;
    }
}

export const fetchUsers = (state = [], action) => {
    switch (action.type) {
        case constants.FETCH_USERS:
            return action.payload;
        default:
            return state;
    }
}

export const fetchUser = (state = null, action) => {
    switch (action.type) {
        case "FETCH_USER":
            return action.payload;
        default:
            return state;
    }
}