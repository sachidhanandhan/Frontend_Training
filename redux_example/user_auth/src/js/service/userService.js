import userApi from '../api/userApi';
import constants from '../util/constants';
import userMessage from '../util/userMessages';
import { push } from 'react-router-redux';
import * as Cookies from '../util/cookieUtil';

export const login = (dispatch, user) => {
    userApi.post(constants.USER_URL_LOGIN, user)
        .then((response) => {
            if (response.status === 200) {
                console.log(response.accessToken);
                Cookies.setCookie('token', response.data.id);
                dispatch(push('user'));
                return dispatch({
                    type: constants.USER_LOGIN,
                    payload: {
                        id: response.data.id,
                        emailId: user.email
                    }
                })
            } else {
                return dispatch({
                    type: constants.USER_LOGIN_FAIL,
                    payload: userMessage.LOGIN_ERROR
                })
            }
        })
        .catch((error) => {
            return dispatch({
                type: constants.USER_LOGIN_FAIL,
                payload: userMessage.LOGIN_FAILED
            })
        })
}

export const fetchUsers = (dispatch, id) => {
    userApi.get(constants.URL, id)
        .then((response) => {
            return dispatch({
                type: constants.FETCH_USERS,
                payload: response.data
            })
        })
        .catch((error) => {
            return dispatch({
                type: constants.FETCH_USERS_ERROR,
                payload: userMessage.FETCH_USER_FAILED
            })
        })
}

export const deleteUserById = (userId, loginId, dispatch) => {
    userApi.delete(constants.URL, userId, loginId)
        .then((response) => {
            return dispatch({
                type: constants.DELETE_SUCCESS,
                payload: "DeleteSuccess"
            })
        })
        .catch((error) => {
            return dispatch({
                type: constants.DELETE_FAIL,
                payload: userMessage.USER_DELETE_FAIL
            })
        })
}

export const fetchUser = (dispatch, userId, loginId) => {
    userApi.get(constants.URL, loginId, userId)
        .then((response) => {
            return dispatch({
                type: "FETCH_USER",
                payload: response.data
            })
        })
        .catch((error) => {
            return dispatch({
                type: "ERROR",
                payload: "can not fetch user, please try again later"
            })
        })
}

export const logoutUser = (id, dispatch) => {
    userApi.postId(constants.USER_URL_LOGOUT, id)
        .then((response) => {
            Cookies.removeCookie('token');
            dispatch(push('/'))
        })
        .catch((error) => {
            return dispatch({
                type: "ERROR",
                payload: "Somthing went wrong please try again later"
            })
        })
}

export const addAUser = (user, id, dispatch) => {
    console.log(user);
    userApi.postId(constants.URL, id, user)
        .then((response) => {
            dispatch(push("user"))
        })
        .catch((error) => {
            console.log("sachin", error);
        })
}