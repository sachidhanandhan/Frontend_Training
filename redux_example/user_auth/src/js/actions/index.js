import { login, fetchUsers, fetchUser, deleteUserById, logoutUser, addAUser } from '../service/userService';
import { push } from 'react-router-redux';

export const userLogin = (user) => {
    return (dispatch) => {
        login(dispatch, user);
    }
}

export const getUsers = (id) => {
    return (dispatch) => {
        fetchUsers(dispatch, id);
    }
}

export const getUser = (userId, id) => {
    return (dispatch) => {
        fetchUser(dispatch, userId, id);
    }
}

export const deleteUser = (id, tokenId) => {
    return (dispatch) => {
        deleteUserById(id, tokenId,dispatch);
    }
}

export const urlNavigate = (name) => {
    return (dispatch) => {
        dispatch(push(`/${name}`));
    }
}

export const logout = (id) => {
    return (dispatch) => {
        logoutUser(id, dispatch);
    }
}

export const addUser = (user, id) => {
    return (dispatch) => {
        addAUser(user, id, dispatch);
    }
}