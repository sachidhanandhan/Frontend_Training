import axios from 'axios';

class UserApi {
    get(url, loginId, userId="") {
        // return axios.get(url +'/'+ userId +'?access_token=' + loginId);
        console.log(`${url}`);
        return axios.get(url);
    }
    post(url, value) {
        console.log(url + value);
        return axios.post(url, value);
    }
    delete(url, id, tokenId) {
        return axios.delete(`${url}/${id}?access_token=${tokenId}`);
    }
    postId(url, id, val) {
        return axios.post(`${url}?access_token=${id}`, val);
    }
}

const userApi = new UserApi();
export default userApi;