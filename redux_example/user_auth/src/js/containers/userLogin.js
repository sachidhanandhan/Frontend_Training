import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { userLogin, urlNavigate } from '../actions/index';
import Login from '../components/login';

function mapStateToProps(state) {
  return {
    loginId: state.loginSuccess,
    message: state.loginFail
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ userLogin: userLogin , navigate: urlNavigate}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);