import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addUser } from '../actions/index';
import AddUser from '../components/addUser';

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ addUser: addUser }, dispatch)
}

export default connect(null, mapDispatchToProps)(AddUser);