import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUser, getUsers, deleteUser, logout, urlNavigate } from '../actions/index';
import Users from '../components/users';

const mapStateToProps = (state) => {
  return {
    users: state.fetchUsers,
    user: state.fetchUser
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getUsers: getUsers, getUser: getUser, deleteUser: deleteUser, logout: logout , navigate: urlNavigate}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);