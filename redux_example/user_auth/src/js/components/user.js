import React, { Component } from 'react';

export default class User extends Component {
    deleteUser(id) {
        this.props.obj.deleteUser(id);
    }

    render() {
        var { user } = this.props.obj;
        if (user) {
            return (
                <div className="row panel">
                    <div className="panel-header"> <h4>User Detail</h4></div>
                    <div className="panel-body">
                        <span>Name: {user.name}</span> <br />
                        <span>EmailId: {user.emailId}</span> <br />
                        <span>PhoneNumber: {user.phoneNumber}</span><br />
                    </div>
                    {/* <div className="panel-footer">
                        <button className="btn btn-danger" onClick={(id) => this.deleteUser(this.props.id)}>Delete</button>
                    </div> */}
                </div>
            );
        } else {
            return (
                <h4>Please select a user..</h4>
            );
        }
    }
}