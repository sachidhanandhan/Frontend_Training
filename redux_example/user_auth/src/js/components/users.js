import React, { Component } from 'react';
import UserList from './userLists';
import User from './user';
import * as Cookies from '../util/cookieUtil';
import { Link } from 'react-router';


export default class Users extends Component {
  constructor(props) {
    super(props);
    if (Cookies.getCookie('token') === undefined) {
      this.props.navigate('/')
    }
  }

  componentDidMount() {
    this.props.getUsers(Cookies.getCookie('token'));
  }

  componentWillReceiveProps() {
    if (Cookies.getCookie('token') === undefined) {
      this.props.navigate('/');
    }
  }

  findUser(id) {
    this.props.getUser(id, Cookies.getCookie('token'));
  }

  deleteUser(id) {
    this.props.deleteUser(id, Cookies.getCookie('token'));
  }

  logout() {
    this.props.logout(Cookies.getCookie('token'));
  }

  render() {
    return (
      <div className="container">
        <div className="panel">
          <div className="panel-header">
          <button className="btn btn-success"><Link to="/addUser">Add user</Link></button>
            <button className="btn btn-danger pull-right" onClick={() => this.logout()}>Logout</button>
          </div>
          <div className="panel-body">
            <div className="row">
              <div className="col-md-6">
                <UserList obj={{ users: this.props.users, findUser: id => this.findUser(id), deleteUser: id => this.deleteUser(id)}} />
              </div>
              <div className="col-md-6">
                <User obj={{ user: this.props.user }} />
              </div>
            </div>
          </div>
        </div>
        <div id="addUser" className="modal fade" role="dialog">
          <div className="modal-dialog">

            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                <h4 className="modal-title">Modal Header</h4>
              </div>
              <div className="modal-body">
                <p>Some text in the modal.</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}