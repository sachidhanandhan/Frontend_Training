import React, { Component } from 'react';

export default class UserLists extends Component {
    getUser(id) {
        this.props.obj.findUser(id);
    }

    deleteUser(id) {
        this.props.obj.deleteUser(id);
    }

    render() {
        var users = this.props.obj.users;
        return (
            <div className="row">
                <div className="col-md-12">
                    <table className="table table-condensed">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>ID</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.map((user) => {
                                return <tr key={user.id}>
                                    <td>{user.name}</td>
                                    <td>{user.id}</td>
                                    <td><button className="btn btn-success" onClick={(id) => this.getUser(user.id)}>View</button></td>
                                    <td><button className="btn btn-danger" onClick={(id) => this.deleteUser(user.id)}>Delete</button></td>
                                </tr>
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}