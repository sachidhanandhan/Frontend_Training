import React, { Component } from 'react';
import * as Cookies from '../util/cookieUtil';

class Login extends Component {
  constructor(props) {
    super(props);
    if (Cookies.getCookie('token')) {
      this.props.navigate('user');
    }

  }

  componentWillReceiveProps() {
    if (Cookies.getCookie('token')) {
      this.props.navigate('user');
    }
  }

  loginAuth(event) {
    event.preventDefault();
    let data = event.target;
    let user = {
      "email": data.elements["email"].value,
      "password": data.elements["password"].value
    }
    this.props.userLogin(user);
  }

  render() {
    return (
      <div className="panel">
        <div className="panel-header">
          <span><h3> Login</h3></span>
        </div>
        <div className="panel-body">
          <form onSubmit={(event) => this.loginAuth(event)}>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Email address:</label>
                  <input type="email" className="form-control" name="email" placeholder="Enter your Email id" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Password:</label>
                  <input type="password" className="form-control" name="password" placeholder="Enter your password" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 text-danger">
                {this.props.message}
              </div>
            </div>
            <div className="row">
              <div className="col-md-2">
                <button type="submit" className="btn btn-success">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <div className="panel-footer">
          <div className="row">
            <div className="col-md-2">
              <button className=" btn btn-success">Facebook login</button>
            </div>
            <div className="col-md-2">
              <button className=" btn btn-danger">google login</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;