import React, { Component } from 'react';
import * as Cookies from '../util/cookieUtil';

class AddUser extends Component {
    addUser(event) {
        event.preventDefault();
        let data = event.target;
        let user = {
            "name": data.elements["name"].value,
            "phoneNumber": data.elements["phoneNumber"].value,
            "email": data.elements["email"].value,
        }
        this.props.addUser(user, Cookies.getCookie('token'));
    }

    componentWillReceiveProps() {
        if (Cookies.getCookie('token') === undefined) {
            this.props.navigate('/');
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-8 col-md-push-2 col-sm-12">
                        <div className="panel">
                            <div className="panel-heading"><div className="panel-title">
                                <label><h4>User Form</h4></label></div>
                            </div>
                            <div className="panel-body">
                                <form id="formValidate" onSubmit={(event) => this.addUser(event)}>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Name</label>
                                                <input type="text" name="name" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Email-Id</label>
                                                <input type="text" name="email" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label >Phone number</label>
                                                <input type="text" name="phoneNumber" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-info btn-pull-right">Create profile</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddUser;