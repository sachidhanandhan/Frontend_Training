export default Object.freeze ({
    LOGIN_FAILED: "Login failed, try again",
    FETCH_USER_FAILED: "Can not fetch users, please try agian later",
    USER_DELETE_FAIL: "User not deleted, please try again later",
    LOGIN_ERROR: "Something went wrong can not login, please try again later"
})