export default Object.freeze({
    URL: "http://localhost:3000/api/userProfiles",
    USER_URL_LOGIN: "http://localhost:3000/api/Users/login",
    USER_URL: "http://localhost:3000/api/Users",
    USER_URL_LOGOUT: "http://localhost:3000/api/Users/logout",
    USER_SELECTED: "userSelected",
    FETCH_USERS: "fetchUsers",
    FETCH_USERS_ERROR: "fetchUsersError",
    USER_LOGIN: "userLogin",
    USER_LOGIN_FAIL: "userLoginFail",
    DELETE_FAIL: "deleteFail",
    DELETE_SUCCESS: "deleteSuccess"
});