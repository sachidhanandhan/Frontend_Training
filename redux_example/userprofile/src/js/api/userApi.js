import axios from 'axios';

class UserApi {
    get(url) {
        return axios.get(url);
    }
    post(url, value) {
        return axios.post(url, value);
    }
    delete(url, id) {
        return axios.delete(url + '/' + id);
    }
}

const userApi = new UserApi();
export default userApi;