import constants from '../util/constants';

export const users = (state = [], action) => {
    switch (action.type) {
        case constants.FETCH_USERS:
            return action.payload;
        case constants.FETCH_USERS_ERROR:
            return action.payload;
        default:
            return state;
    }
}