import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form';
import { users } from './users';
import { activeUser } from './activeUser';

export const userProfile = combineReducers({
    users,
    activeUser,
    form: formReducer
});
