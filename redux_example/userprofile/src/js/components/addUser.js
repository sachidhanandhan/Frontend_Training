import React from 'react';
import { Field, reduxForm } from 'redux-form';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Selects from './selects';
import '../../../node_modules/react-datepicker/dist/react-datepicker.css';

let AddUser = props => {
    const { handleSubmit } = props;
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="name">Name</label>
                <Field name="name" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="email">Email</label>
                <Field name="email" component="input" type="email" />
            </div>
            <div>
                <label htmlFor="phoneno">PhoneNo</label>
                <Field name="phoneNo" component="input" type="text" />
            </div>
            <div>
                <label htmlFor="birthdate">BirthDate</label>
                <Field name="birthDate" placeholder="Enter your date of birth" component={ value =>
                    <DatePicker
                        {...value.input}
                        dateFormat="YYYY-MM-DD"
                        selected={value.input.value ? moment(value.input.value, 'YYYY-MM-DD') : null}
                        onChange={(date) => value.input.onChange(moment(date).format('YYYY-MM-DD'))}
                    />
                } type="text" />
            </div>
            <div>
                <label htmlFor="language">language</label>
                <Field name="language" component={Selects} type="text" />
            </div>
            <button type="submit">Submit</button>
        </form>
    )
}

export default AddUser = reduxForm({ form: 'addUser' })(AddUser)
