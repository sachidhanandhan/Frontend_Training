import Select from 'react-select';
import React from 'react';
import '../../../node_modules/react-select/dist/react-select.css'
import '../../../node_modules/react-select/dist/react-select'

const Selects = props => {
    const options = [
        { value: 'one', label: 'One' },
        { value: 'two', label: 'Two' }
    ];
    var {input} = props;
    return (
        <Select
        {... input}
            name="form-field-name"
            options={options}
            onBlur={() => input.onBlur()}
            simpleValue={true}
        />
    );
}
export default Selects;
