import React from 'react';
import UserList from '../containers/userList';
import User from '../containers/user';
import Add from '../containers/addUser';

const App = () => {
    // fetchUser();
    return [
        <h2 key="usersHeading">Users</h2>,
        <UserList key="users" />,
        <h2 key="heading">User details</h2>,
        <User key="user" />,
        <h2 key="addUser">Add user</h2>,
        <Add />
    ];
}

export default App;
