import UserApi from '../api/userApi';
import Constants from '../util/constants';

export const fetchUsers = () => {
    return (dispatch) => {
        UserApi.get(Constants.URL).then((response) => {
            return dispatch({
                type: Constants.FETCH_USERS,
                payload: response.data
            });
        })
            .catch((err) => {
                return dispatch({
                    type: Constants.FETCH_USERS_ERROR,
                    payload: err
                });
            })
    }
}

export const addUser = (value) => {
    return (dispatch) => {
        UserApi.post(Constants.URL, value)
            .then((response) => {
                 this.fetchUsers();
            })
    }
}

export function selectUser(user) {
    return {
        type: "USER_SELECTED",
        payload: user
    }
}