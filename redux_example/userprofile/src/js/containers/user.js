import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUsers } from '../actions/userAction';

class User extends React.Component {
    componentDidMount() {
        this.props.fetchUsers();
    }

    render() {
        var { user } = this.props;
        return (
            <div>
         {!user ? 'Please select a user' :
            <div>
                <h5>firstname: {user.name}</h5>
                <h5>emailId: {user.emailId}</h5>
                <h5>phoneNumber: {user.phoneNumber}</h5>
            </div>
         }
         </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchUsers: fetchUsers }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(User);