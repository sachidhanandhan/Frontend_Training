import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUsers, addUser } from '../actions/userAction';
import AddUser from '../components/addUser';

class Add extends React.Component {
    componentDidMount() {
        this.props.fetchUsers();
    }
    handleSubmit(a) {
        console.log(a);
        // this.props.addUser(a)
    }
    render() {
        return (
            <AddUser onSubmit={(a) => this.handleSubmit(a)}/>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchUsers: fetchUsers, addUser: addUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Add);
