import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectUser } from '../actions/userAction';

class UserList extends React.Component {
    render() {
        var users = this.props.users;
        return (
            <ul>
                {users && users.map((user, index) => {
                    return <li key={index} onClick={() => this.props.selectUser(user)}>{user.name}</li>
                })
                }
            </ul>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ selectUser: selectUser}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);