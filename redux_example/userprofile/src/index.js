/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/components/app';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import {userProfile} from './js/reducer/index';
import thunk from 'redux-thunk';

const store = createStore(userProfile, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}><App /></ Provider>,
  document.getElementById('root')
);