$(document).ready(function() {
	var users= [];
	$('.datepicker').datepicker();
	$('#formValidate').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			firstName : {
				validators : {
					regexp : {
						regexp : /^[a-zA-Z ]*$/,
						message : 'Name should be in characters'
					},
					notEmpty : {
						message : 'Please enter your First Name'
					}
				}
			},
			lastName : {
				validators : {
					regexp : {
						regexp : /^[a-zA-Z ]*$/,
						message : 'Name should be in characters'
					},
					notEmpty : {
						message : 'Please enter your Last Name'
					}
				}
			},
			DOB : {
				validators : {
					notEmpty : {
						message : 'Please enter valid Date of birth'
					}
				}
			},
			emailId : {
				validators : {
					notEmpty : {
						message : 'Please enter your Email Address'
					},
					emailAddress : {
						message : 'Please enter a valid Email Address'
					}
				}
			},
			phoneNo : {
				validators : {
					regexp : {
						regexp : /^[0-9]*$/,
						message : 'Please check your phone number'
					},
					notEmpty : {
						message : 'Please enter your Contact No.'
					}
				}
			},
			street : {
				validators: {
					notEmpty : {
						message: 'please enter your street address'
					}
				}
			},
			country: {
				validators: {
					notEmpty: {
						message: 'Please enter your country name'
					}
				}
			},
			city: {
				validators: {
					notEmpty: {
						message: 'Please enter your city name'
					}
				}
			},
			postalCode: {
				validators: {
					regexp : {
						regexp : /^[0-9]*$/,
						message: 'please check your postal code'
					},
					notEmpty: {
						message: 'Please enter your postal code'
					}
				}
			}

		}
	}).on('success.form.bv', function(e) {
		e.preventDefault();
		var $form = $(e.target);
		var bv = $form.data('bootstrapValidator');
		saveUser();
	});
	function saveUser() {
		 users.push({
		 	firtstName: $('input[name="firstName"]').val(),
		 	lastName: $('input[name="lastName"]').val(),
		 	DOB: $('input[name="DOB"]').val(),
			phoneNumber: $('input[name="phoneNo"]').val(),
			emailId: $('input[name="emailId"]').val(),
		 	address: {
		 		street: $('input[name="street"]').val(),
		 		city: $('input[name="city"]').val(),
		 		country:$('input[name="country"]').val(),
		 		zipcode: $('input[name="zipCode"]').val()
		 	}
		});
	}
});