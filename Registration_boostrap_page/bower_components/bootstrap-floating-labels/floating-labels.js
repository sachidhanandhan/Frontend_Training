$(function() {
  $("body").on("input propertychange", ".floating-label", function(e) {
    $(this).toggleClass("floating-label-with-value", !!$(e.target).val());
  }).on("focus", ".floating-label", function() {
    $(this).addClass("floating-label-with-focus");
  }).on("blur", ".floating-label", function() {
    $(this).removeClass("floating-label-with-focus");
  });
});
