var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var useref = require('gulp-useref');

gulp.task('sass', function() {
return gulp.src('resources/scss/registrationForm.scss')
.pipe(sass())
.pipe(gulp.dest('resources/css/'))
});

gulp.task('useref', function() {
return gulp.src('*.html')
.pipe(useref())
.pipe(gulp.dest('../'))
});

gulp.task('browserSync', function() {
browserSync.init({
server: {
baseDir: ''
},
})
});

gulp.task('watch', ['sass', 'browserSync'], function() {
gulp.watch('resources/scss/*.scss', ['sass']);
gulp.watch('./*.html', browserSync.reload);
gulp.watch('resources/js/*.js', browserSync.reload);
});

gulp.task('default', ["watch"]);
