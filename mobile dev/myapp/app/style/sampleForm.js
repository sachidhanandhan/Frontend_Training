import {StyleSheet} from 'react-native';

let fontSize = 18,
  primaryColor = '#383547',
  textColor = '#6f6c83';

export default StyleSheet.create({
  container: {
    backgroundColor: primaryColor,
    flex: 1,
    justifyContent: 'space-around'
  },
  textCenter: {
    color: textColor,
    textAlign: 'center',
    paddingTop: 16,
    fontSize: fontSize
  },
  imageView: {
    marginTop: 100,
    alignItems: 'center'
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 50
  },
  formView: {
    flex: 1,
    margin: 20
  },
  inputForm: {
    borderBottomColor: '#615e6f',
    borderBottomWidth: 2,
    flexDirection: 'row',
    marginBottom: 16
  },
  icons: {
    fontSize: fontSize,
    color: textColor,
    marginTop: 16
  },
  input: {
    color: '#c9c6da',
    fontSize: fontSize,
    flex: 1,
    paddingLeft: 10
  },
  footer: {
    textAlign: 'center',
    color: textColor
  }
})