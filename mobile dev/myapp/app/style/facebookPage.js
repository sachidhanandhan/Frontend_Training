import {StyleSheet} from 'react-native';

let borderRadius = 3, 
  componentBackround = '#fafafa',
  defaultColor = '#667fb0',
  fontSize = 22,
  iconColor = '#f3f4f9',
  inactiveColor = '#385185',
  textColor = '#8c92a2',
  textDefaultColor = 'black';

export default StyleSheet.create({
  size: {
    fontSize: fontSize
  },
  component: {
    flex: 1,
    backgroundColor: '#dbdde2'
  },
  headerComponent: {
    flexDirection: 'row',
    backgroundColor: defaultColor,
    elevation: 2,
    padding: 15
  },
  headerMenu: {
    flex: 1,
    paddingLeft: 20
  },
  headerBody: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center'
  },
  headerList: {
    flex: 1,
    alignItems: 'flex-end'
  },
  userOptions: {
    flexDirection: 'row',
    padding: 14,
    elevation: 4,
    backgroundColor: '#fefefe'
  },
  userOptionContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  userOptionIcon: {
    paddingRight: 6,
    fontSize: fontSize
  },
  newsFeed: {
    margin: 20,
    padding: 10,
    flexDirection: 'row',
    backgroundColor: '#fefefe',
    borderRadius: borderRadius,
    elevation: 1
  },
  newsFeedText: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  newsFeedChange: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  icon: {
    paddingRight: 30,
    fontSize: fontSize
  },
  feedText: {
    fontSize: 22,
    color: textDefaultColor,
    paddingLeft: 10,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  userPost: {
    backgroundColor: '#fefefe',
    elevation: 2,
    borderRadius: borderRadius,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20
  },
  postHeader: {
    flexDirection: 'row',
    padding: 10
  },
  postImage: {
    height: 50,
    width: 50
  },
  postName: {
    fontWeight: 'bold',
    fontSize: 18,
    color: textDefaultColor
  },
  postHeaderText: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 3
  },
  postHeaderIcon: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  postGlobe: {
    paddingLeft: 5,
    fontSize: 14
  },
  postBody: {
    padding: 10
  },
  postBodyText: {
    color: textDefaultColor,
    fontSize: 18
  },
  postFooter: {
    padding: 10,
    borderTopWidth: 1,
    borderColor: '#ffffff',
    flexDirection: 'row',
    backgroundColor: '#fafafa',
    paddingBottom: 10
  },
  footerIconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  footerIcons: {
    color: '#9b9ea3',
    paddingRight: 5,
    fontSize: 14
  },
  footerText: {
    fontWeight: 'bold',
    fontSize: 16
  }
});
