import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import Icon from 'react-native-fa-icons';
import styles from '../../style/facebookPage';

export default class FacebookPosts extends Component {
  render() {
    let data = this.props.data;
    return (
      <View>
        <View style={[styles.userPost]}>
          <View style={styles.postHeader}>
            <Image source={require('../../image/sachin.jpg')} style={styles.postImage} />
            <View style={styles.postHeaderTextContainer}>
              <Text style={styles.postName}>{data.name}</Text>
              <View style={styles.postHeaderText}>
                <Text>Just now</Text>
                <Icon name="globe" style={styles.postGlobe}/>
              </View>
            </View>
            <View style={styles.postHeaderIcon}>
              <Icon name="sort-desc" style={styles.size} />
            </View>
          </View>
          <View style={styles.postBody}>
            <Text style={styles.postBodyText}>
              {data.message}
            </Text>
          </View>
          <View style={styles.postFooter}>
            <View style={styles.footerIconContainer}>
              <Icon name="thumbs-o-up" style={styles.footerIcons}/>
              <Text style={styles.footerText}> Like </Text>
            </View>
            <View style={styles.footerIconContainer}>
              <Icon name="comment-o" style={styles.footerIcons}/>
              <Text style={styles.footerText}> Comment </Text>
            </View>
            <View style={styles.footerIconContainer}>
              <Icon name="share" style={styles.footerIocns}/>
              <Text style={styles.footerText}> Share </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

