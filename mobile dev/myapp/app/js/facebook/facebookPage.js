import React, {Component} from 'react';
import {View, Text, FlatList} from 'react-native';
import Icon from 'react-native-fa-icons';
import styles from '../../style/facebookPage';
import FacebookPosts from './facebookPosts';

export default class FacebookPage extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        id: 1,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 2,
        name: 'Sachidhanandhan',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 3,
        name: 'Kosalraman',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 4,
        name: 'jegadesh',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 5,
        name: 'Pandirajen',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 6,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 7,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 8,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 9,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 10,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 11,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 12,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 13,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 14,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 15,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 16,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 17,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      },
      {
        id: 18,
        name: 'Sachusivakumar',
        message: 'Sunday morning conversation around the cinnamons rolls: '
      }
    ]
  }
  render() {
    return (
      <View style={styles.component}>
        <View style={styles.headerComponent}>
          <View style={styles.headerMenu}>
            <Icon name="bars" style={styles.icon}/>
          </View>
          <View style={styles.headerBody}>
            <Icon name="globe" style={styles.icon}/>
            <Icon name="users" style={styles.icon} />
            <Icon name="comments" style={styles.icon}/>
          </View>
          <View style={styles.headerList}>
            <Icon name="server" style={styles.icon}/>
          </View>
        </View>
        <View style={styles.userOptions}>
          <View style={styles.userOptionContainer}>
            <Icon name="external-link" style={styles.userOptionIcon}/>
            <Text> Status </Text>
          </View>
          <View style={styles.userOptionContainer}>
            <Icon name="camera" style={styles.userOptionIcon}/>
            <Text>Photo</Text>
          </View>
          <View style={styles.userOptionContainer}>
            <Icon name="check-square-o" style={styles.userOptionIcon}/>
            <Text> Check In</Text>
          </View>
        </View>
        <FlatList data={this.data}
          renderItem={({item}) =>
            <FacebookPosts data={item}/>
          }
          keyExtractor={item => item.id}/>
      </View>
    );
  }
}

