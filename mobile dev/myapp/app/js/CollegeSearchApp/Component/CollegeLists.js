import React, { Component } from 'react';
import { FlatList, Text, View, TextInput, Picker, ActivityIndicator } from 'react-native';
import Service from '../Service/collegeService';
import styles from '../Styles/collegeSearch';
import Icon from 'react-native-fa-icons';
import { List, ListItem } from 'react-native-elements';

export default class CollegeLists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collegeLists: [],
      searchValue: '',
      searchOption: 'name',
      collegeListCount: '',
      refreshing: false,
      pageNo: 1
    }
  }
  componentDidMount() {
    if (this.state.collegeLists.length === 0) {
      this.getCollegeLists();
    }
  }
  render() {
    return (
      <View>
        <View style={styles.searchMenu}>
          <View style={styles.searchComponent}>
            <TextInput placeholder="Search Colleges"
              value={this.state.searchValue}
              onChangeText={text => this.setState({searchValue: text})}
              style={styles.searchInput}
              underlineColorAndroid= "transparent"/>
            <Icon name="search" style={styles.searchIcon} onPress={this.searchCollege} />
          </View>
          <Picker selectedValue={this.state.searchOption}
            onValueChange={value => this.setState({searchOption: value})}
            style={styles.selectOption}>
            <Picker.Item label="College" value="name" />
            <Picker.Item label="Country" value="country" />
          </Picker>
        </View>
        <Text>{this.state.pageNo}</Text>
        {this.state.data &&
          <List containerStyle={{marginTop: 0, borderTopWidth: 0, borderBottomWidth: 0}}>
            <FlatList data={this.state.data}
              renderItem={({item}) =>
                <Text> {item.name}</Text>
              }
              keyExtractor={item => item.name}
              ListFooterComponent={this.renderFooter}
              onEndReached={this.getPaginatedLists}
              onEndReachedThreshold={0.1}
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          </List> }
        {this.state.collegeListCount === 0 &&
        <Text style={styles.searchResult}> No College Found </Text>
        }
      </View>
    );
  }
  onRefresh = () => {
    this.getCollegeLists();
  }
  renderFooter = () => {
    console.log("asdasd");
    return (
      <View>
        <Text> ssadada</Text>
        <ActivityIndicator animating size="large" />
      </View>
    );
  }
  getCollegeLists = () => {
    this.setState({refreshing: true});
    Service.lists((error, result) => {
      if (!error) {
        var data = [];
        var totalLength;
        result.map((college, index) => {
          if (index < 60) {
            data.push(college);
            totalLength = index;
          }
        });
        this.setState({
          collegeLists: result,
          data: data,
          refreshing: false,
          totalLength: totalLength,
          pageNo: 1
        });
      }
    });
  }
  searchCollege = () => {
    if (this.state.searchValue !== '') {
      this.setState({
        collegeLists: [],
        collegeListCount: ''
      });
      Service.searchCollege({
        value: this.state.searchValue,
        option: this.state.searchOption
      }, (error, result) => {
        if (!error) {
          this.setState({
            collegeLists: result,
            searchValue: '',
            collegeListCount: result ? result.length : 0,
            pageNo: 1
          });
        }
      });
    }
  }
  getPaginatedLists = () => {
    let currentSize = this.state.totalLength;
    let maximumSize = currentSize + 10;
    let pageNo = this.state.pageNo + 1;
    var data = [...this.state.data];
    this.state.collegeLists.map((college, index) => {
      if (index > currentSize && index < maximumSize) {
        data.push(college);
      }
    });
    this.setState({
      data: data,
      pageNo: pageNo,
      totalLength: maximumSize
    });
  }
}
