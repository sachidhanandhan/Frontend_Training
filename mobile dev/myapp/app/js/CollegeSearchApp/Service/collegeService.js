module.exports = {
  lists: function(callback) {
    return fetch('http://universities.hipolabs.com/search')
      .then(response => response.json()).then(result => callback(null, result))
      .catch(error => callback(error));
  },
  searchCollege: function(params, callback) {
    let value = params.value || '';
    let option = params.option || '';
    console.log(params);
    return fetch(`http://universities.hipolabs.com/search?${option}=${value}`)
      .then(response => response.json())
      .then(result => callback(null, result))
      .catch(error => callback(error));
  }
}

