import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  searchMenu: {
    flexDirection: 'row',
    padding: 10
  },
  searchComponent: {
    flex: 2,
    flexDirection: 'row',
    borderBottomColor: 'green',
    borderBottomWidth: 1,
    paddingRight: 10
  },
  searchInput: {
    flex: 2,
    paddingBottom: 0
  },
  selectOption: {
    flex: 1
  },
  searchIcon: {
    fontSize: 20,
    color: 'green',
    paddingTop: 16
  },
  searchResult: {
    paddingTop: 20,
    textAlign: 'center'
  }
})