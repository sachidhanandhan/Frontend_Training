import {StyleSheet} from 'react-native';

const 
  iconSize = 18,
  inboxIcon = '#0078d4',
  textColor = '#5a5a5a',
  textSize = 20;
export default StyleSheet.create({
  todoLists: {
    flexDirection: 'column',
    flex: 1
  },
  todoList: {
    flexDirection: 'row',
    paddingLeft: 10
  },
  inboxIcon: {
    flex: 1,
    fontSize: iconSize,
    color: inboxIcon
  },
  todoValue: {
    flex: 5,
    fontSize: textSize,
    color: textColor
  },
  count: {
    flex: 1,
    color: '#a8a8a8',
    fontSize: 10
  },
  plusIcon: {
    flex: 1,
    fontSize: iconSize,
    color: inboxIcon
  },
  createList: {
    color: inboxIcon,
    flex: 6,
    fontSize: textSize
  }
});
