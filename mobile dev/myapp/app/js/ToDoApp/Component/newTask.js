import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-fa-icons';
import styles from '../styles/newTask';

export default class NewContact extends Component {
  render() {
    return (
      <View style={styles.todoLists}>
        <View style={styles.todoList}>
          <Icon name="inbox" style={styles.inboxIcon}/>
          <Text style={styles.todoValue}>Inbox</Text>
          <Text style={styles.count}>3</Text>
        </View>
        <View style={styles.todoList}>
          <Icon name="inbox" style={styles.inboxIcon}/>
          <Text style={styles.todoValue}>Inbox</Text>
          <Text style={styles.count}>3</Text>
        </View>
        <View style={styles.todoList}>
          <Icon name="plus" style={styles.plusIcon} />
          <Text style={styles.createList}>Create list</Text>
        </View>
      </View>
    );
  }
}
