import React, {Component} from 'react';
import {Text, View, TextInput, Image, Button} from 'react-native';
import styles from '../style/sampleForm';
import Icon from 'react-native-fa-icons';

export default class SampleForm extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageView}>
          <Image source={require('../image/sachin.jpg')} style={styles.image}/>
          <Text style={styles.textCenter}> Login Form </Text>
        </View>
        <View style={styles.formView}>
          <View style={styles.inputForm}>
            <Icon style={styles.icons} name="envelope-o" />
            <TextInput style={styles.input}
              placeholderTextColor={'#6f6c83'}
              underlineColorAndroid={'transparent'}
              placeholder="Enter your email address" />
          </View>
          <View style={styles.inputForm}>
            <Icon style={styles.icons} name="lock" />
            <TextInput style={styles.input}
              underlineColorAndroid={'transparent'}
              placeholderTextColor={'#6f6c83'}
              placeholder="Enter your password" />
          </View>
          <Button color="#33303f" title="Log in" onPress={() => this.onPress()} />
          <Text style={styles.textCenter}> Forgot your password </Text>
        </View>
        <View style={styles.footerView}>
          <Text style={styles.footer}> Don't have an account? Create one</Text>
        </View>
      </View>
    );
  }
}
