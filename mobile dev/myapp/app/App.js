/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
// import TodoApp from './js/ToDoApp/Component/newTask';
// import SampleForm from './js/sampleForm';
import CollegeLists from './js/CollegeSearchApp/Component/CollegeLists';
import FacebookPage from './js/facebook/facebookPage';

export default class App extends Component {
  render() {
    return (
      // <TodoApp />
      <CollegeLists />
      // <SampleForm />
    );
  }
}
